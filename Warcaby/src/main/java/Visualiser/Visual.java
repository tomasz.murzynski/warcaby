/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visualiser;

import pl.ecokorporacja.warcaby.Board;
import static pl.ecokorporacja.warcaby.Main.whoPlay;
import pl.ecokorporacja.warcaby.Player;
import pl.ecokorporacja.warcaby.Position;

/**
 *
 * @author Tomasz Murzynski
 */
public class Visual {

    public static void printBoard(Board board) {
        Character[][] table = board.getTable();
        System.out.println("   0 1 2 3 4 5 6 7");
        int i = 0;
        for (Character[] characters : table) {
            System.out.print(i + " |");
            i++;
            for (Character character : characters) {
                System.out.print(character);
                System.out.print("|");
            }
            System.out.print("\n");
        }
    }

    public static void askStartPositionX(Player[] player) {
        System.out.println(player[whoPlay].getName() + " what is pawn's position?? Enter row :");
    }

    public static void askStartPositionY(Player[] player) {
        System.out.println(player[whoPlay].getName() + " what is pawn's position?? Enter col :");
    }

    public static void askEndPositionX(Player[] player) {
        System.out.println(player[whoPlay].getName() + ". Attack position?? Enter X :");
    }

    public static void askEndPositionY(Player[] player) {
        System.out.println(player[whoPlay].getName() + ". Attack position?? Enter X :");
    }

    public static void printEmptyPosition() {
        System.out.println("There is no pawn in the given position.");
    }

    public static void printOpponentPawn() {
        System.out.println("This is the pawn of the opponent.");
    }

    public static void printWhoPlay(Player[] player) {
        System.out.println("Move player " + player[whoPlay].getName() + " with signature " + player[whoPlay].getSignN() + " and " + player[whoPlay].getSignQ());

    }

    public static void printWrongMove() {
        System.out.println("Wrong move !!!");
        
    }
    
    public static void wrongMoveData(Position[] position) {
        System.out.println("position[1].getX() - position[0].getX()==" + (position[1].getX() - position[0].getX()));
        System.out.println("position[1].getY() - position[0].getY() ==" + (position[1].getY() - position[0].getY()));
    }

    public static void printPosibleMove() {
        System.out.println("Posible move.");
    }

    public static void printOutOfBoard() {
        System.out.println("Out of the board !!!");
    }

    public static void printGoodMove() {
        System.out.println("Good move.");
    }
}
//            //Sprawdzanie zawartości tablicy o współrzędnych position[0]{x,y}
//            System.out.println("position[1] X:  " + position[1].getX() + " Y: " + position[1].getY());
//
//            //Czy pozycja na planszy
//            System.out.println("Czy jest na planszy = " + position[1].onTheBoard(board, position));
//
//            //Co zawiera pozycja
//            System.out.println("Position[1] on board sign =\'" + board.getSignFromPosition(board, position[1]) + "\'");
//
//            //Sprawdz czy atak jesli tak to czy możliwy następny atak
//            System.out.println("Is attack = " + isAttack(position_, board));