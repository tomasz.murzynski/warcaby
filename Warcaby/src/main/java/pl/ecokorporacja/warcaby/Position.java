/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.ecokorporacja.warcaby;

import Visualiser.Visual;

/**
 *
 * @author Tomasz Murzynski
 */
public class Position {

    //Współrzędne x i y
    int[] position = new int[2];
    int x;
    int y;
    
    public Position(){
        
    }

    public Position(int[] position) {
        this.position = position;
        this.x = position[0];
        this.y = position[1];
    }

    void setPositionXY(int x, int y) {
        this.x = x;
        this.y = y;
        position[0] = x;
        position[1] = y;
    }
    
    void setPositionX(int x) {
        this.x = x;        
        position[0] = x;       
    }
    
    void setPositionY(int y) {
        this.y = y;
        position[1] = y;
    }

    public int[] getPosition() {
        return position;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    boolean onTheBoard(Board board, Position[] position){
        
        int size;
        size = board.getFINAL_SIZE();
        if (position[1].x >= 0 && position[1].x < size && position[1].y >= 0 && position[1].y < size){
            return true;
        }else {
            
            Visual.printOutOfBoard();
            return false;
        }
    }
    
    
}
