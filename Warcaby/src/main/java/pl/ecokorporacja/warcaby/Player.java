/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.ecokorporacja.warcaby;

import java.util.Scanner;

/**
 *
 * @author Tomasz Murzynski
 */
public class Player extends Board {
    

    //Scanner scanner = new Scanner(System.in);
    private String name;
    private char[] sign = new char[2];

    public Player() {

    }

    public Player(String name, char[] sign) {
        this.name = name;
        this.sign = sign;
    }

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public char getSignN() {
        return sign[0];
    }
    
    public char getSignQ() {
        return sign[1];
    }

    void setName(String name) {
        this.name = name;
    }

    void setSign(char signatureNormal, char signatureQuine ) {
        this.sign[0] = signatureNormal;
        this.sign[1] = signatureQuine;
    }

//    void createPlayer(int numberOfPlayers, Player[] player, Scanner scanner) {
//
//        for (int i = 0; i < numberOfPlayers; i++) {
//            player[i] = new Player();
//            System.out.println("Podaj imię gracza (White) : ");
//            player[i].setName(scanner.nextLine());
//        }
//        
//
//        //Zmiana na enum 
//        EnumPawn pawn = EnumPawn.NORMAL;
//        EnumPawn superPawn = EnumPawn.QUINE;
//        char[] newSign = new char[2];
//        newSign[0] = pawn.getPlayerWhite();
//        newSign[1] = superPawn.getPlayerWhite();
//        player[0].setSign(newSign);
//    }

}
