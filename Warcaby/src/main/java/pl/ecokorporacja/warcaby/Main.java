/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.ecokorporacja.warcaby;

import Visualiser.Visual;
import java.util.Scanner;
import static pl.ecokorporacja.warcaby.PawnNormal.isAttack;

/**
 *
 * @author Tomasz Murzynski
 */
public class Main {

    public static int whoPlay = 0;
    public static int enemyPlayer = 1;
    public static Pawn pawn;

    public static void main(String[] args) {

        //Create players
        Scanner scanner = new Scanner(System.in);
        Player[] player = new Player[2];

        createPlayer(player, scanner);

        //Print players info
        printPlayerInfo(player);

        //Board
        //Create new board 
        Board board = new Board();
        board.createBoard();
        board.clearBoardFromNull();
        board.setPawnWhite();
        board.setPawnBlack();

        //Print who play
        Visual.printWhoPlay(player);

        //Move
        Position[] position = new Position[2];
        position[0] = new Position();
        position[1] = new Position();
        while (true) {
            move(pawn, player, board, position, scanner);            
            switchPlayer();
        }
    }

    public static boolean possibleMove(int whoPlay, Position[] position, Board board) {
        if (!board.isAttackInRound()) {
            if (whoPlay == 0) {
                if ((position[1].getX() - position[0].getX() == 1)
                        && ((position[1].getY() - position[0].getY() == -1) || (position[1].getY() - position[0].getY() == 1))) {
                    Visual.printPosibleMove();  
                    return true;
                } //Dla bicia
                else if (isAttack(position, board)
                        && (position[1].getX() - position[0].getX() == 2)
                        && ((position[1].getY() - position[0].getY() == -2) || (position[1].getY() - position[0].getY() == 2))) {
                    Visual.printPosibleMove();  
                    return true;
                } else {
                    Visual.printWrongMove();
                    Visual.wrongMoveData(position);
                    return false;
                }
            } //Dla drugiego playera
            else {
                if (position[1].getX() - position[0].getX() == -1
                        && ((position[1].getY() - position[0].getY() == -1) || (position[1].getY() - position[0].getY() == 1))) {
                    Visual.printPosibleMove();  
                    return true;
                } else if (isAttack(position, board)
                        && (position[1].getX() - position[0].getX() == -2)
                        && ((position[1].getY() - position[0].getY() == -2) || (position[1].getY() - position[0].getY() == 2))) {
                    Visual.printPosibleMove();  
                    return true;
                } else {
                    Visual.printWrongMove();
                    Visual.wrongMoveData(position);
                    return false;
                }
            }
        } //Bicie w przelocie
        else {
            if ((position[1].getX() - position[0].getX() == 1)
                    && ((position[1].getY() - position[0].getY() == -1) || (position[1].getY() - position[0].getY() == 1))) {
                Visual.printPosibleMove();  
                return true;
            }
            else if (isAttack(position, board)
                    && (position[1].getX() - position[0].getX() == 2)
                    && ((position[1].getY() - position[0].getY() == -2) || (position[1].getY() - position[0].getY() == 2))) {
                Visual.printPosibleMove();  
                return true;
            } else if (position[1].getX() - position[0].getX() == -1
                    && ((position[1].getY() - position[0].getY() == -1) || (position[1].getY() - position[0].getY() == 1))) {
                Visual.printPosibleMove();  
                return true;
            } else if (isAttack(position, board)
                    && (position[1].getX() - position[0].getX() == -2)
                    && ((position[1].getY() - position[0].getY() == -2) || (position[1].getY() - position[0].getY() == 2))) {
                Visual.printPosibleMove();    
                return true;
            } else {
                Visual.printWrongMove();
                Visual.wrongMoveData(position);
                return false;
            }

        }

    }

    public static void getPositionToAttack(Board board, Position[] position, Scanner scanner, Pawn pawn, Player[] player) {
        do {
            Visual.printBoard(board);
            Visual.askEndPositionX(player);
            position[1].setPositionX(scanner.nextInt());
            Visual.askEndPositionY(player);
            position[1].setPositionY(scanner.nextInt());            
        } while (!possibleMove(whoPlay, position, board));

    }

    public static void move(Pawn pawn, Player[] player_, Board board, Position[] position_, Scanner scanner) {
        getPawnForMove(board, position_, player_, scanner);
        do {            
            checkTheTypeOfThePiece(pawn, player_, board, position_);
            getPositionToAttack(board, position_, scanner, pawn, player_);

            //Wykonaj ruch
            swichPlacePawn(board, position_[0], position_[1]);

            //zmiana na quine jeśli spełnione
            changePawnToQuine(player_[whoPlay], board, position_);                      
            position_[0].setPositionXY(position_[1].getX(), position_[1].getY());
        } while (board.isAttackInRound() && checkIfAnotherPunchPosible(position_, board, player_));
        Visual.printGoodMove();
    }

    public static boolean checkIfAnotherPunchPosible(Position[] position_, Board board, Player[] player) {

        boolean result = false;

        //Pozycje możliwego wroga
        Position enemy1 = new Position();
        Position enemy2 = new Position();
        Position enemy3 = new Position();
        Position enemy4 = new Position();

        //Lewy górny róg
        enemy1.setPositionXY(position_[1].x - 1, position_[1].y - 1);

        //Lewy dolny róg
        enemy3.setPositionXY(position_[1].x + 1, position_[1].y - 1);

        //Prawy górny róg
        enemy2.setPositionXY(position_[1].x - 1, position_[1].y + 1);

        //Prawy dolny róg
        enemy4.setPositionXY(position_[1].x + 1, position_[1].y + 1);

        //Pozycje pustego pola 
        Position free1 = new Position();
        Position free2 = new Position();
        Position free3 = new Position();
        Position free4 = new Position();

        //Lewy górny róg
        free1.setPositionXY(position_[1].x - 2, position_[1].y - 2);
        //Lewy dolny róg
        free3.setPositionXY(position_[1].x + 2, position_[1].y - 2);
        //Prawy górny róg
        free2.setPositionXY(position_[1].x - 2, position_[1].y + 2);
        //Prawy dolny róg
        free4.setPositionXY(position_[1].x + 2, position_[1].y + 2);

        //Sprawdzamy czy są na planszy
        //Lewy górny róg
        if (free1.onTheBoard(board, position_)) {
            if (enemy1.onTheBoard(board, position_)) {
                result = true;
                System.out.println("Possible anotcher move!!!");
            }
        } //Lewy dolny róg
        else if (free3.onTheBoard(board, position_)) {
            if (enemy3.onTheBoard(board, position_)) {
                result = true;
                System.out.println("Possible anotcher move!!!");
            }
        } //Prawy górny róg
        else if (free2.onTheBoard(board, position_)) {
            if (enemy2.onTheBoard(board, position_)) {
                result = true;
                System.out.println("Possible anotcher move!!!");
            }
        } //Prawy dolny róg
        else if (free4.onTheBoard(board, position_)) {
            if (enemy4.onTheBoard(board, position_)) {
                result = true;
                System.out.println("Possible anotcher move!!!");
            }
        }
        System.out.println("checkIfAnotherPunchPosible" + result);
        return result;
    }

    public static void changePawnToQuine(Player player, Board board, Position[] position) {
        if (position[1].x == 0 || position[1].x == 7) {

            switchToQuine(player, board, position[1]);
            System.out.println("Switch pawn to quine");
        }
    }

    public static void switchToQuine(Player player, Board board, Position position) {
        board.setPlayerSign(player.getSignQ());
        board.setQuineToPosition(board, position);
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Tu coś jest nie tak
    public static void swichPlacePawn(Board board, Position positionStart, Position positionStop) {
        board.setTempSign(board.getSignFromPosition(board, positionStart));
        board.clearPosition(board, positionStart);
        board.setPawnToPosition(board, positionStop);
        positionStart = positionStop;
    }

    public static void checkTheTypeOfThePiece(Pawn pawn, Player[] player, Board board, Position[] position) {

        //pionek normalny
        if (player[whoPlay].getSignN() == board.getSignFromPosition(board, position[0])) {
            pawn = new PawnNormal();
        } //pionek quine
        else if (player[whoPlay].getSignQ() == board.getSignFromPosition(board, position[0])) {
            pawn = new PawnQuine();
        } else {
            System.out.println("on this position isn't pawn");
        }
    }

    public static void getPawnForMove(Board board, Position[] position, Player[] player, Scanner scanner) {
        do {
            //Wydrukuj szachownice
            Visual.printBoard(board);
            //Pobieranie współrzędnych
            Visual.askStartPositionX(player);
//            System.out.println(player[whoPlay].getName() +" what is pawn's position?? Enter row :");
            position[0].setPositionX(scanner.nextInt());
            Visual.askStartPositionY(player);
//            System.out.println(player[whoPlay].getName() +" what is pawn's position?? Enter col :");
            position[0].setPositionY(scanner.nextInt());

//            //Sprawdzanie zawartości tablicy o współrzędnych position[0]{x,y}
//            System.out.println("position[0] X:  " + position[0].getX() + " Y: " + position[0].getY());
//            System.out.println("Position[0] on board sign =\'" + board.getSignFromPosition(board, position[0]) + "\'");
//            System.out.println("Pozycja zaakceptowana !!!");
        } while (!equalsPlayerSign(player[whoPlay], board, position[0]));
    }

    public static boolean ifPawnExistOnPosition(Board board, Position position) {
        if (' ' != board.getSignFromPosition(board, position)) {
            return true;
        } else {
            Visual.printEmptyPosition();
//            System.out.println("There is no pawn in the given position.");
            return false;
        }
    }

    public static boolean equalsPlayerSign(Player player, Board board, Position position) {
        if (player.getSignN() == board.getSignFromPosition(board, position)
                || player.getSignQ() == board.getSignFromPosition(board, position)) {
            return true;
        } else {
            return false;
        }
    }

    boolean isPlayersPawn(Player player, Board board, Position position) {
        if (player.getSignN() == board.getSignFromPosition(board, position)
                || player.getSignQ() == board.getSignFromPosition(board, position)) {
            return true;
        } else {
            Visual.printOpponentPawn();
//            System.out.println("This is the pawn of the opponent.");
            return false;
        }
    }

    private static void switchPlayer() {
        if (whoPlay == 0) {
            whoPlay = 1;
            enemyPlayer = 0;
        } else {
            whoPlay = 0;
            enemyPlayer = 1;
        }
    }

    private static void printPlayerInfo(Player[] player) {
        System.out.println("White name : " + player[0].getName() + " with pawn : " + player[0].getSignN() + " and pawn qunine " + player[0].getSignQ());
        System.out.println("Black name : " + player[1].getName() + " with sign : " + player[1].getSignN() + " and pawn qunine " + player[1].getSignQ());
    }

    private static void createPlayer(Player[] player, Scanner scanner) {
        //Player white;
        player[0] = new Player();
        System.out.println("Podaj imię gracza (White) : ");
        player[0].setName(scanner.nextLine());
        Signature signWhite = Signature.WHITE;
        player[0].setSign(signWhite.getPawnNormal(), signWhite.getPawnQuine());

        //Player black
        player[1] = new Player();
        System.out.println("Podaj imię gracza (Black) : ");
        player[1].setName(scanner.nextLine());
        Signature signBlack = Signature.BLACK;
        player[1].setSign(signBlack.getPawnNormal(), signBlack.getPawnQuine());
    }
}
