/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.ecokorporacja.warcaby;

/**
 *
 * @author Tomasz Murzynski
 */
public enum Signature {
    
    WHITE('w','W'),
    BLACK('b','B');
    
    private final char pawnNormal;
    private final char pawnQuine;

    private Signature(char pawnNormal, char pawnQuine) {
        this.pawnNormal = pawnNormal;
        this.pawnQuine = pawnQuine;
    }

    public char getPawnNormal() {
        return pawnNormal;
    }

    public char getPawnQuine() {
        return pawnQuine;
    }
    
}


//public enum EnumPawn {
//    NORMAL('w','b'),
//    QUINE('W','B');
//    
//    private char playerWhite;
//    private char playerBlack;
//    
//    EnumPawn(char pWhite, char pBlack){
//        playerWhite = pWhite;
//        playerBlack = pBlack;
//    }
//    
//    public char getPlayerWhite(){
//        return playerWhite;
//    }
//    
//    public char getPlayerBlack(){
//        return playerBlack;
//    }
//    
//}
