/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.ecokorporacja.warcaby;

import java.util.Arrays;
import static pl.ecokorporacja.warcaby.Main.whoPlay;

/**
 *
 * @author Tomasz Murzynski
 */
public class PawnNormal extends Pawn {
    
    public static boolean isAttack(Position[] position_, Board board) {

        boolean result = false;
        Position enemyPosition = new Position();
        Signature sign;

        System.out.println("wgoPlay = " + whoPlay);
        //System.out.println("sing = " + Signature.BLACK);
        System.out.println("position_[0] = " + Arrays.toString(position_[0].position));
        System.out.println("position_[1] = " + Arrays.toString(position_[1].position));
        //System.out.println("sing = " + Signature.BLACK);

        //Player white
        if (whoPlay == 0) {

            sign = Signature.BLACK;
            System.out.println("sing = " + sign);
            //Atak w prawo
            //Pozycja startowa y > pozycja końcowa y
            if (position_[0].y > position_[1].y) {
                //Jeśli na pozycji o {1,-1} wiekszej niż pozycja startowa jest pionek przeciwnika                
                enemyPosition.setPositionXY(position_[0].x + 1, position_[0].y - 1);

                System.out.println("enemyPosition = " + (position_[0].x + 1) + "," + (position_[0].y - 1));

                if (sign.getPawnNormal() == board.getSignFromPosition(board, enemyPosition)
                        || sign.getPawnQuine() == board.getSignFromPosition(board, enemyPosition)) {
                    //board.setAttackInRound(true);
                    board.clearPosition(board, enemyPosition);

                    System.out.println("Jest atak 1");

                    result = true;
                }
            } //Atak w lewo
            //Pozycja startowa y < pozycja końcowa y
            else if (position_[0].y < position_[1].y) {
                //Jeśli na pozycji o {1,1} wiekszej niż pozycja startowa jest pionek przeciwnika   
                enemyPosition.setPositionXY(position_[0].x + 1, position_[0].y + 1);
                if (sign.getPawnNormal() == board.getSignFromPosition(board, enemyPosition)
                        || sign.getPawnQuine() == board.getSignFromPosition(board, enemyPosition)) {
                    //board.setAttackInRound(true);
                    board.clearPosition(board, enemyPosition);

                    System.out.println("Jest atak 2");

                    result = true;
                }
            }
        } //Player black
        else {
            sign = Signature.WHITE;
            System.out.println("sing = " + sign);
            //Pozycja starowa y > pozycja końcowa y  (bicie w lewo)
            if (position_[0].y > position_[1].y) {
                //Jeśli na pozycji o {-1,-1} wiekszej niż pozycja startowa jest pionek przeciwnika   
                enemyPosition.setPositionXY(position_[0].x - 1, position_[0].y - 1);
                if (sign.getPawnNormal() == board.getSignFromPosition(board, enemyPosition)
                        || sign.getPawnQuine() == board.getSignFromPosition(board, enemyPosition)) {
                    //board.setAttackInRound(true);
                    board.clearPosition(board, enemyPosition);

                    System.out.println("Jest atak 3");

                    result = true;
                }
            } else if (position_[0].y < position_[1].y) {
                //Jeśli na pozycji o {-1,+1} wiekszej niż pozycja startowa jest pionek przeciwnika   
                enemyPosition.setPositionXY(position_[0].x - 1, position_[0].y + 1);
                if (sign.getPawnNormal() == board.getSignFromPosition(board, enemyPosition)
                        || sign.getPawnQuine() == board.getSignFromPosition(board, enemyPosition)) {
                    //board.setAttackInRound(true);
                    board.clearPosition(board, enemyPosition);

                    System.out.println("Jest atak 4");

                    result = true;
                }
            }
        }

        System.out.println("Result = " + result);
        board.setAttackInRound(result);
        return result;
    }
    
}

