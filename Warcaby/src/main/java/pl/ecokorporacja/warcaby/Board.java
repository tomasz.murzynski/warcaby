/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.ecokorporacja.warcaby;

/**
 *
 * @author Tomasz Murzynski
 */

public class Board {

    private final int FINAL_SIZE;
    private final char FINAL_SIGN_FREE = ' ';
    private Character[][] table;        
    private char tempSign;
    private char playerSign;
    private boolean attackInRound;
    
    public char getFINAL_SIGN_FREE() {
        return FINAL_SIGN_FREE;
    }

    public char getTempSign() {
        return tempSign;
    }
    

    public boolean isAttackInRound() {
        return attackInRound;
    }

    public void setAttackInRound(boolean attackInRound) {
        this.attackInRound = attackInRound;
    }

    public char getPlayerSign() {
        return playerSign;
    }

    public void setPlayerSign(char playerSign) {
        this.playerSign = playerSign;
    }

    public void setTempSign(char tempSign) {
        this.tempSign = tempSign;
    }
    
    public Board() {        
        this.FINAL_SIZE = 8;
        this.table = new Character[FINAL_SIZE][FINAL_SIZE];
    }

    public int getFINAL_SIZE() {
        return FINAL_SIZE;
    }

    public void setTable(Character[][] table) {
        this.table = table;
    }

    public Character[][] getTable() {
        return table;
    }
    
    public char getSignFromPosition(Board board, Position position) {        
        return table[position.x][position.y];        
    } 
    
    public void setQuineToPosition(Board board, Position positionStop){
        table[positionStop.x][positionStop.y] = board.getPlayerSign();
    }
    
    public void setPawnToPosition(Board board, Position positionStop){
        table[positionStop.x][positionStop.y] = board.getTempSign();
    }
    
    public void clearPosition(Board board, Position position){
        table[position.x][position.y] = FINAL_SIGN_FREE;
    }

    void createBoard() {
        table = new Character[FINAL_SIZE][FINAL_SIZE];
    }

    void clearBoardFromNull() {
        for (Character[] table1 : table) {
            for (int m = 0; m < table.length; m++) {
                table1[m] = ' ';
            }
        }
    }

    void setPawnWhite() {
        Signature sign = Signature.WHITE;        
        for (int i = 0; i < 3; i++) {
            for (int m = 0; m < table.length; m = m + 2) {
                if (i % 2 == 0) {
                    table[i][m] = sign.getPawnNormal();
                } else {
                    table[i][m + 1] = sign.getPawnNormal();
                }
            }
        }
    }

    void setPawnBlack() {
        Signature sign = Signature.BLACK;
        for (int i = table.length; table.length - 2 <= i; i--) {
            for (int m = 0; m < table.length; m = m + 2) {
                if (i % 2 == 0) {
                    table[i - 1][m + 1] =  sign.getPawnNormal(); 
                } else {
                    table[i - 1][m] = sign.getPawnNormal(); 
                }
            }
        }                        
    }    
}
