/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.ecokorporacja.warcaby;

import java.util.Scanner;

/**
 *
 * @author Tomasz Murzynski
 */
public class Move extends Player {
//    Scanner scanner = new Scanner(System.in);
    
    int[] fromVector2 = new int[2];
    int[] destinyVector2 = new int[2];
            
    public Move(){
        
    }
    
    //
    

    public int[] getFromVector2() {
        return fromVector2;
    }

    public void setFromVector2(int[] fromVector2) {
        this.fromVector2 = fromVector2;
    }

    public int[] getDestinyVector2() {
        return destinyVector2;
    }

    public void setDestinyVector2(int[] destinyVector2) {
        this.destinyVector2 = destinyVector2;
    }
}
